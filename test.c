/*
 * Test the functionality of the DS1340 RTC and its software drivers.
 * 
 * File:   test.c
 * Author: alek
 *
 * Created on 08 February 2018, 09:37
 */

// CONFIG BITS
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF        // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

#define _XTAL_FREQ     20000000

// Includes.
#include <xc.h>
#include "DS1340.h" // The RTC driver.
#include "uart.h" // TenkouUART lib needed for debug trace.

/*
 * Various defines specific for debugging and TK-01-01-01 hardware.
 */
// Bus frequencies and timing definitions.
#define UART_BAUD 9600 // UART baud rate in bits per second.
#define I2C_FREQ 100000 // I2C frequency in Hz.
#define MAIN_LOOP_WAIT 1000 // How long to wait after every main loop in ms.

// TK-01-01-01 OBC PCB hardware-specific pin definitions.
#define MCU_BCU_RESET RD4 // Used by the MCU to reset the BCU and vice-versa.
#define MCU_BCU_RESET_INIT TRISD4
#define RTC_I2C_ENABLE RD0 // Pull high to enable RTC level shifter.
#define RTC_I2C_ENABLE_INIT TRISD0


/*
 * Data buffers used for debugging.
 */
unsigned char loopStart[] = {"START"};
unsigned char rtcMsg[]={"RTC"}; // Make the RTC data stand out.
unsigned char errData[] = {"ERR"}; // Error notifier.
unsigned char uartTerminator[] = {"XXX\n"}; // Distinct end of debug messages.
// For the received time data: second, minutes, hours, date.
unsigned char rtcRData[6]={1,2,3,4};
// Will set the RTC to this initial time: second, minutes, hours, dateday.
unsigned char rtcTData[6]={0x7,0x50,0x23,0x10}; // ~10 minutes from a change of day.

/*
 * Debugging functions.
 */
// Convert between RTC date format and binary (numbers) back and forth.
inline uint8_t bcd2bin(uint8_t val){return val - 6 * (val >> 4);}
inline uint8_t bin2bcd(uint8_t val){return val + 6 * (val / 10);}

//TODO this function doesn't work when only it is called in the main loop. It spams UART.
int printTimeToUART(unsigned char* dateTime)
/* Print the dateTime array with seconds, minutes, hours, day, month, year
 * to UART separating with the 1st byte of uartTerminator. UART has to be
 * initialised before calling this function. Returns 0 if the operation was
 * successful.
 *
 * This is only a test function, so it isn't included in the flight software.
 */
{
    uint8_t temp=0; // Time converted form BCD to binary.
    uartWriteBytes(rtcMsg,3);
    for(unsigned char i=0;i<6;i++) // For each dateTime element.
    {
        temp=bcd2bin((uint8_t) dateTime[i]);
        if(temp<10) // Time component is only one digit.
        {
            char buf[1]; // Will cast into this. Need buffer for two digits.
            utoa(buf,temp,10);
            uartWriteBytes(buf,1); // Send this time component.
        }
        else if(temp<100) // Need a larger buffer but time value still makes sense.
        {
            char buf[2];
            utoa(buf,temp,10);
            uartWriteBytes(buf,2);
        }
        else // Exceeded the buffer size, RTC data are wrong.
        {
            uartWriteBytes(errData,3);
        }
        uartWriteBytes(uartTerminator,1); // Only show one separator between readings.
    }
    return 0;
}

int test_rtc_1(void){ /* Test setting and reading the RTC in a loop. */
    // Initialise pins. Both are outputs.
    MCU_BCU_RESET_INIT=0;
    RTC_I2C_ENABLE_INIT=0;
    
    // Disable the other PIC (needed when testing in TK-01-01-01 hardware).
    MCU_BCU_RESET=0;
    
    i2cMasterInit(I2C_FREQ); // Start I2C @ 100 kHz frequency (supported by the RTC).
    
    // Show the beginning of the main loop by sending a string.
    uartInit(UART_BAUD); // Initialise UART @ 9600 baud.
    
    // Test setting the RTC - set an initial time.
    __delay_ms(1000); // Arbitrary, let the RTC start up before resetting the time.
    RTC_I2C_ENABLE=1;
    uint8_t ret = (uint8_t) setRTC(rtcTData);
    RTC_I2C_ENABLE=0;
    
    // Test reading the RTC - see how the time changes over, well, time.
    for(;;) // Main loop.
    {
        RTC_I2C_ENABLE=1; // Only communicate with the RTC with the level shifter.
        ret = (uint8_t) readRTC(rtcRData); // Get a time vector from the RTC.
        RTC_I2C_ENABLE=0; // Done with the RTC & the level shifter.
        uartWriteBytes(rtcRData,4); // Print raw data - easier to verify if the RTC works.
        //TODO printTimeToUART spams the UART for seconds>=10
        //ret = (uint8_t) printTimeToUART(rtcRData); // Print the time to UART.

        uartWriteBytes(uartTerminator,4); // Distinct end of the data message.
        __delay_ms(MAIN_LOOP_WAIT); // Wait for the RTC to change the time it's showing.
    }
}

int main(void) {
    int a = test_rtc_1();
}
