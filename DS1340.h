/* 
 * Functions used to set and read the time from the DS1340 RTC.
 * 
 * File:   DS1340.h
 * Author: alek
 *
 * Created on 08 February 2018, 09:38
 */

#ifndef DS1340_H // Header safeguard.
#define	DS1340_H

#include "i2c_master.h" // TenkouI2CMaster lib needed for I2C bus control.
#include <stdint.h> // uint8_t
#include <xc.h>

// Basic setting and reading of time.
int readRTC(unsigned char* dateTime);
int setRTC(unsigned char* dateTime);
int readRTCRegister(unsigned char* outputLocationPtr, unsigned char* addressPtr);
int setRTCRegister(unsigned char* timeBytePtr, unsigned char* addressPtr);

int rtc_initialization(uint8_t* time_data);
int rtc_error(void);
int rtc_read(uint8_t* dateTime);

// RTC registers.
unsigned char rtcWriteAddr = 0xD0; // 7-bit (1101000) RTC address plus write 0 at the end.
unsigned char rtcReadAddr = 0xD1; // RTC address plus read 1 at the end.
unsigned char rtcSecsAddr = 0x00; // RTC seconds register address.
unsigned char rtcMinsAddr = 0x01; // Minutes.
unsigned char rtcHrsAddr = 0x02; // Hours.
unsigned char rtcDateAddr = 0x04; // Date.
unsigned char rtcMonthAddr = 0x05; // Month.
unsigned char rtcYearAddr = 0x06; // Year.
unsigned char rtcflagAddr = 0x09; // RTC flag register address.

#endif	/* DS1340_H */

