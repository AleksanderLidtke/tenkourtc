# README #

### What is this repository for? ###

This is a set of functions to interact with the DS1340 real-time clock (RTC) on-board
the Ten-Koh satellite. The RTC is located in the TK-01-01-01 OBC PCB and can be accessed
on the I2C bus controlled by two PIC16F877 PICs, the main and backup controller units
(MCU and BCU, respectively).

### How do I get set up? ###

Just clone this repository as well as the [TenkouI2C Master library](https://bitbucket.org/rafarodleon/tenkoui2cmaster)
that is used to send and receive data on the I2C bus.

There should be separate header and source files with all the functions needed to read
and set the RTC time, as well as a set of dedicated test functions in a separate `test.c` file.
The header and source files will be included in the OBC software, while the test functions
will only be used to test the code and the hardware on the ground.

### Who do I talk to? ###

For more info, talk to Alek or Urakami on Slack.