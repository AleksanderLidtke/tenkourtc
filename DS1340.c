/* 
 * Functions used to set and read the time from the DS1340 RTC.
 * 
 * File:   DS1340.c
 * Author: alek
 *
 * Created on 08 February 2018, 09:37
 */

#include "DS1340.h"

int readRTCRegister(unsigned char* outputLocationPtr, unsigned char* addressPtr)
/* Read one register of the RTC under the given address and store the
 * returned byte at the outputLocationPtr memory location. Return 0 at the end.
 */
{
    // Set the register pointer to the chosen location.
    if (!i2cMasterStart()) // Issue the I2C start condition.
        return -1;
    i2cMasterWrite(&rtcWriteAddr); // Select the RTC by writing to it.
    i2cMasterWrite(addressPtr); // Write to set the register pointer.
    i2cMasterStop(); // For some reason need this here, otherwise the I2C transaction won't stop.
    
    // Read a byte from the selected register.
    i2cMasterStart(); // Issue the start condition.
    i2cMasterWrite(&rtcReadAddr); // Address byte = first byte after start condition.
    i2cMasterRead(0,outputLocationPtr); // Put the read byte where the user wants it.
    i2cMasterStop(); // Stop the I2C communication.
    
    return 0; // Done.
}

int readRTC(unsigned char* dateTime)
/* Get the current time from the RTC and record the read date and time in a vector
 * with the following entries: second, minute, hour, date.
 * Month and year have never been tested because we (Alek and Rafa) don't think
 * we actually need them.
 * Return 0 if the read was successful.
 */
{
    if (readRTCRegister(dateTime,&rtcSecsAddr))
        return -1;
    readRTCRegister(dateTime+1,&rtcMinsAddr);
    readRTCRegister(dateTime+2,&rtcHrsAddr);
    readRTCRegister(dateTime+3,&rtcDateAddr);
    readRTCRegister(dateTime+4,&rtcMonthAddr);
    readRTCRegister(dateTime+5,&rtcYearAddr);
    return 0;
}

int setRTCRegister(unsigned char* timeBytePtr, unsigned char* addressPtr)
/* Write one byte to a register of the RTC under the given address. 
 * Return 0 at the end.
 */
{
    // Writing a byte into the register can be done in one go, as opposed to
    // reading that seems to require a write to set the register pointer to the
    // desired address, and then a read from that register.
    i2cMasterStart(); // Issue the I2C start condition.
    i2cMasterWrite(&rtcWriteAddr); // Select the RTC by writing to it.
    i2cMasterWrite(addressPtr); // Write to set the register pointer.
    i2cMasterWrite(timeBytePtr); // Write the actual data into the register.
    i2cMasterStop(); // Stop the I2C communication.
    
    return 0; // Done.
}

int setRTC(unsigned char* dateTime)
/* Set the RTC time registers according to the time bytes given in dateTime
 * with the following entries: second, minute, hour, date.
 * Months and years have never been tested because we (Alek and Rafa) think
 * that we don't actually need them. 
 * Return 0 at the end if everything goes well. */
{
    setRTCRegister(dateTime,&rtcSecsAddr);
    setRTCRegister(dateTime+1,&rtcMinsAddr);
    setRTCRegister(dateTime+2,&rtcHrsAddr);
    setRTCRegister(dateTime+3,&rtcDateAddr);
    setRTCRegister(dateTime+4,&rtcMonthAddr);
    setRTCRegister(dateTime+5,&rtcYearAddr);
    
    return 0;
}

int rtc_error(void) {
    i2cMasterStart();
    i2cMasterWrite(&rtcWriteAddr);
    i2cMasterWrite(&rtcflagAddr);
    i2cMasterRepeatedStart();
    i2cMasterWrite(&rtcReadAddr);
    unsigned char* flag;
    i2cMasterRead(0,flag);
    i2cMasterStop();
    return (*flag == 0x80) ? 0 : 1;
}

int rtc_initialization(uint8_t* time_data) {
    if(rtc_error()) {
        i2cMasterStart();
        i2cMasterWrite(&rtcWriteAddr);
        i2cMasterWrite(&rtcSecsAddr);
        
        for(int shift_address = 0 ; shift_address == 6 ; shift_address++) {
            i2cMasterWrite(time_data + shift_address);
        }
        i2cMasterStop();
        return -1;
    }
    return 0;
}

int rtc_read(uint8_t* dateTime) {
    if(rtc_error()) {
        i2cMasterStart();
        i2cMasterWrite(&rtcReadAddr);
        for(int shift_address = 0 ; shift_address == 6 ; shift_address++) {
            i2cMasterRead(0 , dateTime + shift_address);
        }
        i2cMasterStop();
        return -1;
    }
    return 0;
}